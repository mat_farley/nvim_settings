# nvim_settings
Just some nvim configs for some pleb

clone recursively b/c the plugins are submodules

`git clone --recursive https://github.com/mathewf2/nvim_settings.git ~/.config/nvim` for linux

`git clone --recursive https://github.com/mathewf2/nvim_settings.git ~/AppData/Local/nvim` for windows


update submodules

`git submodule update --recursive --remote`

when adding a new plugin, from within `plugged/`

`git add submodule <url>`

alternatively

`git add submodule <url> plugged/`

need to install vim-plug (`init.vim` should do this automatically), and need Node >= 12.12 for coc.nvim install
